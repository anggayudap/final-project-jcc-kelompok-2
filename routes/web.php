<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    /* ROUTE utama */
    Route::get('/', 'DashboardController@index');
});

/* ROUTE untuk Update User */
Route::get('/users', 'UserController@index');
Route::put('/users/{users_id}', 'UserController@update');

/* ROUTE untuk Auth */
Auth::routes();

/* Route untuk mempelai */
Route::resource('mempelai', 'MempelaiController');
/* Route untuk undangan */
Route::resource('undangan', 'UndanganController');
/* Route untuk tamu undangan */
Route::resource('tamu', 'TamuController');
Route::get('/tamu/{nomor_hp_tamu}/send_wa', 'TamuController@send_wa');
/* Route untuk media undangan */
Route::resource('media', 'MediaController');

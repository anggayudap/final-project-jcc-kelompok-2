<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Undangan extends Model {
    protected $table = "undangan";
    protected $fillable = ["tanggal", "waktu_mulai", "waktu_selesai", "alamat", "jenis_template", "longtitude_maps", "latitude_maps", "id_user"];

}

<?php

namespace App\Providers;

use App\Undangan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        ga bisa :(
//        $id_user = Auth::user()->id;
//        $undangan = Undangan::where('id_user', $id_user)->first();
//        View::share('id_undangan_global', $undangan->id);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = "konten_undangan";
    protected $fillable = ["type", "filename", "id_undangan"];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tamu extends Model
{
    protected $table = "tamu_undangan";
    protected $fillable = ["nama_tamu", "alamat_tamu", "nomor_hp_tamu", "id_undangan"];

}

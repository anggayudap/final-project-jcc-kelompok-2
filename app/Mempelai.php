<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mempelai extends Model {
    protected $table = "mempelai";
    protected $fillable = ["nama_mempelai", "nama_lengkap_mempelai", "jenis_kelamin", "nama_bapak_mempelai", "nama_ibu_mempelai", "status_anak", "id_undangan"];
}

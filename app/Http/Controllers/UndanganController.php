<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Undangan;
use Illuminate\Support\Facades\Auth;

class UndanganController extends Controller {
    public function index() {
        $id_user = Auth::user()->id;
        $undangan = Undangan::where('id_user', $id_user)->first();
        if ($undangan->count() > 0) {
            $undangan = $undangan;
        } else {
            $undangan = null;
        }
        return view('undangan.create', compact('undangan'));
    }

    public function create() {
        $id_user = Auth::user()->id;
        $undangan = Undangan::where('id_user', $id_user)->first();
        if ($undangan->count() > 0) {
            $undangan = $undangan;
        } else {
            $undangan = null;
        }
        return view('undangan.create', compact('undangan'));
    }

    public function store(Request $request) {
//        $this->validate($request, [
//            'tanggal'        => 'required',
//            'waktu_mulai'    => 'required',
//            'waktu_selesai'  => 'required',
//            'alamat'         => 'required',
//            'jenis_template' => 'required',
//        ]);
//
//        Undangan::create([
//            'tanggal'         => $request->tanggal,
//            'waktu_mulai'     => $request->waktu_mulai,
//            'waktu_selesai'   => $request->waktu_selesai,
//            'alamat'          => $request->alamat,
//            'jenis_template'  => $request->jenis_template,
//            'longtitude_maps' => $request->longtitude_maps,
//            'latitude_maps'   => $request->latitude_maps,
//            'id_user'         => $request->id_user,
//        ]);
//
//        return redirect('/undangan');
    }

    public function show($id) {
    }

    public function edit($id) {
    }

    public function update(Request $request, $id) {
        $request->validate([
            'tanggal'        => 'required',
            'waktu_mulai'    => 'required',
            'waktu_selesai'  => 'required',
            'alamat'         => 'required',
        ]);

        $tamu = Undangan::find($id);
        $tamu->tanggal = $request->tanggal;
        $tamu->waktu_mulai = $request->waktu_mulai;
        $tamu->waktu_selesai = $request->waktu_selesai;
        $tamu->alamat = $request->alamat;
        $tamu->jenis_template = 'a';
        $tamu->longtitude_maps = $request->longtitude_maps;
        $tamu->latitude_maps = $request->latitude_maps;
        $tamu->update();
        return redirect('/undangan');
    }

    public function destroy($id) {
    }
}

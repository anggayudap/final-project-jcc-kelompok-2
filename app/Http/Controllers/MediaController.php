<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Undangan;

class MediaController extends Controller {
    public function __construct() {
        session(['key' => 'value']);
    }

    public function index() {
        $media = Media::all();
        return view('media.index', compact('media'));
    }

    public function create() {
        return view('media.create');
    }

    public function store(Request $request) {
        $id_user = Auth::user()->id;
        $undangan = Undangan::where('id_user', $id_user)->first();

        $this->validate($request,[
            'type' => 'required',
            'media'  => 'required|mimes:jpg,jpeg,png',
        ]);
        $file_foto = $request->file('media');
        $new_foto  = time().' - '.$file_foto->getClientOriginalName();
        Media::create([
            "type" => $request["type"],
            "filename" => $new_foto,
            "id_undangan"  => $undangan->id,
        ]);

        $file_foto->move('assets/images/media/', $new_foto);

        return redirect('/media');
    }

    public function show($id) {

    }

    public function edit($id) {

    }

    public function update(Request $request, $id) {

    }

    public function destroy($id) {
        $media = Media::find($id);
        $media->delete();
        return redirect('/media');
    }
}

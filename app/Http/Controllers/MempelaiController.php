<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mempelai;
use App\Undangan;
use Illuminate\Support\Facades\Auth;

class MempelaiController extends Controller {
    public function __construct() {

    }

    public function index() {
        $id_user = Auth::user()->id;
        $undangan = Undangan::where('id_user', $id_user)->first();
        $mempelai_pria = Mempelai::where([['id_undangan', $undangan->id],['jenis_kelamin', 'laki-laki']])->first();
        $mempelai_wanita = Mempelai::where([['id_undangan', $undangan->id],['jenis_kelamin', 'perempuan']])->first();

        if ($mempelai_pria) {
            $mempelai['pria'] = $mempelai_pria;
        } else {
            $mempelai['pria'] = null;
        }
        if ($mempelai_wanita) {
            $mempelai['wanita'] = $mempelai_wanita;
        } else {
            $mempelai['wanita'] = null;
        }

        return view('mempelai.create', compact('mempelai'));
    }

    public function create() {
        return view('mempelai.create');
    }

    public function store(Request $request) {
        $id_user = Auth::user()->id;
        $undangan = Undangan::where('id_user', $id_user)->first();

        $this->validate($request, [
            '1_nama_lengkap_mempelai' => 'required',
            '1_nama_mempelai'         => 'required',
            '1_jenis_kelamin'         => 'required',
            '1_nama_bapak_mempelai'   => 'required',
            '1_nama_ibu_mempelai'     => 'required',
            '1_status_anak'           => 'required|numeric',
            '2_nama_lengkap_mempelai' => 'required',
            '2_nama_mempelai'         => 'required',
            '2_jenis_kelamin'         => 'required',
            '2_nama_bapak_mempelai'   => 'required',
            '2_nama_ibu_mempelai'     => 'required',
            '2_status_anak'           => 'required|numeric',
        ]);

        Mempelai::create([
            "nama_lengkap_mempelai" => $request["1_nama_lengkap_mempelai"],
            "nama_mempelai"         => $request["1_nama_mempelai"],
            "jenis_kelamin"         => $request["1_jenis_kelamin"],
            "nama_bapak_mempelai"   => $request["1_nama_bapak_mempelai"],
            "nama_ibu_mempelai"     => $request["1_nama_ibu_mempelai"],
            "status_anak"           => $request["1_status_anak"],
            "id_undangan"           => $undangan->id,
        ]);
        Mempelai::create([
            "nama_lengkap_mempelai" => $request["2_nama_lengkap_mempelai"],
            "nama_mempelai"         => $request["2_nama_mempelai"],
            "jenis_kelamin"         => $request["2_jenis_kelamin"],
            "nama_bapak_mempelai"   => $request["2_nama_bapak_mempelai"],
            "nama_ibu_mempelai"     => $request["2_nama_ibu_mempelai"],
            "status_anak"           => $request["2_status_anak"],
            "id_undangan"           => $undangan->id,
        ]);

        return redirect('/mempelai');
    }
}

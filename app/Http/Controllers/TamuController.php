<?php

namespace App\Http\Controllers;

use App\Undangan;
use Illuminate\Http\Request;
use App\Tamu;
use Illuminate\Support\Facades\Auth;

class TamuController extends Controller {
    public function index() {
        $tamu = Tamu::all();
        return view('tamu.index', compact('tamu'));
    }

    public function create() {
        return view('tamu.create');
    }

    public function store(Request $request) {
        $id_user = Auth::user()->id;
        $undangan = Undangan::where('id_user', $id_user)->first();

        $this->validate($request, [
            'nama_tamu'     => 'required',
            'alamat_tamu'   => 'required',
            'nomor_hp_tamu' => 'required',
        ]);

        Tamu::create([
            "nama_tamu"     => $request["nama_tamu"],
            "alamat_tamu"   => $request["alamat_tamu"],
            "nomor_hp_tamu" => $request["nomor_hp_tamu"],
            "id_undangan"   => $undangan->id,
        ]);

        return redirect('/tamu');
    }

    public function show($id) {
        // ga dipake
    }

    public function edit($id) {
        $tamu = Tamu::find($id);
        return view('tamu.edit', compact('tamu'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'nama_tamu'     => 'required',
            'nomor_hp_tamu' => 'required',
        ]);

        $tamu = Tamu::find($id);
        $tamu->nama_tamu = $request->nama_tamu;
        $tamu->alamat_tamu = $request->alamat_tamu;
        $tamu->nomor_hp_tamu = $request->nomor_hp_tamu;
        $tamu->update();
        return redirect('/tamu');
    }

    public function destroy($id) {
                $tamu = Tamu::find($id);
                $tamu->delete();
                return redirect('/tamu');
    }

    public function send_wa($nomor_hp_tamu) {
        echo "<script>window.open('https://wa.me/" . $nomor_hp_tamu . "', '_blank')</script>";
//        return redirect('/tamu');
    }
}

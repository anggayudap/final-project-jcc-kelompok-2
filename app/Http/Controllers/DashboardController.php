<?php

namespace App\Http\Controllers;

use App\Media;
use App\Mempelai;
use App\Tamu;
use App\Undangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class DashboardController extends Controller {
    protected $site_settings;

    public function __construct() {
        session(['key' => 'value']);
    }

    public function index() {
        $output = [];
        $output['media'] = false;
        $output['mempelai'] = false;
        $output['undangan'] = false;
        $output['undangan_anda'] = false;
        $output['tamu'] = false;

        $data_user = Auth::user();
        $undangan = Undangan::where('id_user', $data_user->id)->first();
        $mempelai_pria = Mempelai::where([['id_undangan', $undangan->id], ['jenis_kelamin', 'laki-laki']])->first();
        $mempelai_wanita = Mempelai::where([['id_undangan', $undangan->id], ['jenis_kelamin', 'perempuan']])->first();
        $media = Media::where('id_undangan', $undangan->id);
        $tamu = Tamu::where('id_undangan', $undangan->id);


        $output['user'] = $data_user->name;
        /* check data mempelai */
        if ($mempelai_pria && $mempelai_wanita) {
            $output['mempelai'] = true;
        }

        /* check data undangan */
        if ($undangan->tanggal && $undangan->waktu_mulai && $undangan->waktu_selesai && $undangan->alamat) {
            $output['undangan'] = true;
        }

        /* check data media && lebih dari 3 */
        if ($media) {
            if ($media->count() >= 3) {
                $output['media'] = true;
            }
        }

        /* check data tamu */
//        dd($tamu->get());
        if ($tamu) {
            $output['tamu'] = true;
        }

        /* check data undangan anda */
        if ($output['mempelai'] && $output['undangan'] && $output['media']) {
            $output['undangan_anda'] = true;
        }

        return view('dashboard.index', compact('output'));
    }
}

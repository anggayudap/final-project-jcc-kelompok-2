<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Hash;

class UserController extends Controller
{
    public function index()
    {
        
        $users = DB::table('users')->where('id', Auth::user()->id)->first();
        return view('users.index', compact('users'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        $query = DB::table('users')
            ->where('id', $id)
            ->update([
                "name" => $request["name"],
                "email" => $request["email"],
                "password" => Hash::make($request["password"]),
            ]);
        return redirect('/users');
    }
}

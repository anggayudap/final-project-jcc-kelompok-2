<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="{{asset('assets/images/user.png')}}" class="img-circle elevation-2"
                 alt="User Image">
        </div>
        <div class="info">
            <a href="/" class="d-block">{{ Auth::user()->name }}</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
            data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>Dashboard </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/mempelai" class="nav-link">
                    <i class="nav-icon fas fa-user"></i>
                    <p>Data Mempelai </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/tamu" class="nav-link">
                    <i class="nav-icon fas fa-user-friends"></i>
                    <p>Data Tamu </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/undangan" class="nav-link">
                    <i class="nav-icon fas fa-envelope-open"></i>
                    <p>Data Undangan </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/media" class="nav-link">
                    <i class="nav-icon fas fa-photo-video"></i>
                    <p>Media Undangan </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="/users" class="nav-link">
                    <i class="nav-icon fas fa-user-edit"></i>
                    <p>Profil User </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('logout') }}" class="nav link"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    <i class="nav-icon fas fa-power-off"></i>
                    <p>{{ __('Logout') }}</p>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>

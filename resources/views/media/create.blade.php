@extends('layout.admin.master')

@section('title-menu')
    Tambah Media Undangan
@endsection

@section('content')
    <div>
        <h2>Tambah Data</h2>
        <form action="/media" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Tipe Media</label>
                <select class="form-control col-6" name="type">
                    <option value="foto">Foto</option>
                    <option value="video">Video</option>
                </select>
                @error('type')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Media</label>
                <input type="file" class="form-control col-6" id="media" name="media">
                @error('media')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
@endsection

@push('scripts')
    <script>

    </script>
@endpush

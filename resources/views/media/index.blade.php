@extends('layout.admin.master')

@section('title-menu')
    Data Media Undangan
@endsection

@section('content')
    <a href="/media/create" class="btn btn-primary">Tambah</a>
    <table id="example1" class="table table-bordered table-striped">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tipe</th>
            <th scope="col">Preview</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($media as $key=>$value)
            <tr>
                <td>
                {{$key + 1}}</th>
                <td>{{$value->type}}</td>
                <td><a href="#" onclick="buka_gambar('{{$value->filename}}')">
                        <img class="img-fluid col-4" src="{{asset('assets/images/media/'.$value->filename)}}"
                             alt="Photo">
                    </a></td>
                <td>
                    <form action="/media/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="5">
                <td>No data</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{-- modal --}}
    <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Preview Media</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
    </script>
    <script src="{{asset('assets/templates/admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/templates/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();

        });

        function buka_gambar(url) {
            $('#modal-lg').modal('toggle');

            $('div.modal-body').html('');
            $('div.modal-body').append('<img class="img-fluid col-12" src="' + site_url + '/assets/images/media/' + url + '" alt="Photo">');
        }
    </script>
@endpush

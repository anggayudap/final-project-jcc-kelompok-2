@extends('layout.admin.master')

@section('title-menu')
    Data Mempelai
@endsection

@section('content')
    <div>
        <form action="/mempelai" method="POST" class="horizontal" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title"><i class="nav-icon fas fa-user mr-2"></i>Mempelai Pria</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="1_id" value>
                            <input type="hidden" name="1_jenis_kelamin" value="laki-laki">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"
                                           value="{{($mempelai['pria']) ? $mempelai['pria']->nama_lengkap_mempelai : ''}}"
                                           name="1_nama_lengkap_mempelai" placeholder="Nama Lengkap">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Panggilan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"
                                           value="{{($mempelai['pria']) ? $mempelai['pria']->nama_mempelai : ''}}"
                                           name="1_nama_mempelai"
                                           placeholder="Nama Panggilan">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Bapak</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"
                                           value="{{($mempelai['pria']) ? $mempelai['pria']->nama_bapak_mempelai : ''}}"
                                           name="1_nama_bapak_mempelai" placeholder="Nama Orangtua Laki-Laki">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Ibu</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"
                                           value="{{($mempelai['pria']) ? $mempelai['pria']->nama_ibu_mempelai : ''}}"
                                           name="1_nama_ibu_mempelai" placeholder="Nama Orangtua Perempuan">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Status Anak</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="1_status_anak"
                                           value="{{($mempelai['pria']) ? $mempelai['pria']->status_anak : ''}}"
                                           placeholder="Anak ke-">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card card-pink">
                        <div class="card-header">
                            <h3 class="card-title"><i class="nav-icon fas fa-user mr-2"></i>Mempelai Wanita</h3>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="2_id" value>
                            <input type="hidden" name="2_jenis_kelamin" value="perempuan">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"
                                           value="{{($mempelai['wanita']) ? $mempelai['wanita']->nama_lengkap_mempelai : ''}}"
                                           name="2_nama_lengkap_mempelai" placeholder="Nama Lengkap">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Panggilan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="2_nama_mempelai"
                                           value="{{($mempelai['wanita']) ? $mempelai['wanita']->nama_mempelai : ''}}"
                                           placeholder="Nama Panggilan">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Bapak</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"
                                           value="{{($mempelai['wanita']) ? $mempelai['wanita']->nama_bapak_mempelai : ''}}"
                                           name="2_nama_bapak_mempelai" placeholder="Nama Orangtua Laki-Laki">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Ibu</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control"
                                           value="{{($mempelai['wanita']) ? $mempelai['wanita']->nama_ibu_mempelai : ''}}"
                                           name="2_nama_ibu_mempelai" placeholder="Nama Orangtua Perempuan">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Status Anak</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="2_status_anak"
                                           value="{{($mempelai['wanita']) ? $mempelai['wanita']->status_anak : ''}}"
                                           placeholder="Anak ke-">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
@endsection

@push('scripts')
    <script>
    </script>
@endpush

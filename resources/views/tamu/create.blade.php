@extends('layout.admin.master')

@section('title-menu')
    Tambah Data Tamu Undangan
@endsection

@section('content')
    <div>
        <h2>Tambah Data</h2>
        <form action="/tamu" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama_tamu" id="nama_tamu"
                       placeholder="Masukkan Nama">
                @error('nama_tamu')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Alamat Tamu</label>
                <textarea class="form-control" name="alamat_tamu" id="alamat_tamu"
                          placeholder="Masukkan Alamat Tamu"></textarea>
                @error('alamat_tamu')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Nomor HP (ex: 6282122001234)</label>
                <input type="text" class="form-control" name="nomor_hp_tamu" id="nomor_hp_tamu"
                       placeholder="Masukkan Nomor HP (contoh: 6282122001234)">
                @error('nomor_hp_tamu')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
@endsection

@push('scripts')
    <script>
    </script>
@endpush

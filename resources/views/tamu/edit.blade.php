@extends('layout.admin.master')

@section('title-menu')
    Kelola Data Tamu
@endsection

@section('content')
    <div>
        <h2>Edit Tamu {{$tamu->id}}</h2>
        <form action="/tamu/{{$tamu->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama_tamu" value="{{$tamu->nama_tamu}}" id="nama_tamu"
                       placeholder="Masukkan Nama">
                @error('nama_tamu')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Alamat Tamu</label>
                <textarea class="form-control" name="alamat_tamu" id="alamat_tamu"
                          placeholder="Masukkan Alamat Tamu">{{$tamu->alamat_tamu}}</textarea>
                @error('alamat_tamu')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Nomor HP (ex: 6282122001234)</label>
                <input type="text" class="form-control" name="nomor_hp_tamu" value="{{$tamu->nomor_hp_tamu}}" id="nomor_hp_tamu"
                       placeholder="Masukkan Nomor HP (contoh: 6282122001234)">
                @error('nomor_hp_tamu')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection

@push('scripts')

@endpush

@extends('layout.admin.master')

@section('title-menu')
    Data Tamu Undangan
@endsection

@section('content')
    <a href="/tamu/create" class="btn btn-primary">Tambah</a>
    <table id="example1" class="table table-bordered table-striped">
        <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Tamu</th>
            <th scope="col">Alamat</th>
            <th scope="col">Nomor HP</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($tamu as $key=>$value)
            <tr>
                <td>
                {{$key + 1}}</th>
                <td>{{$value->nama_tamu}}</td>
                <td>{{$value->alamat_tamu}}</td>
                <td>{{$value->nomor_hp_tamu}}</td>
                <td>
                    <a href="/tamu/{{$value->nomor_hp_tamu}}/send_wa" class="btn btn-info">Send</a>
                    <a href="/tamu/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/tamu/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="5">
                <td>No data</td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection

@push('scripts')
    <script>
        console.log('hello JCC. ini adalah datatable');
    </script>
    <script src="{{asset('assets/templates/admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/templates/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
@endpush

@extends('layout.admin.master')
@push('styles')
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
@endpush
@section('title-menu')
    Dashboard
@endsection

@section('content')
    <div class="row">
        <div class="col-3 ">
            <div class="small-box bg-info">
                <div class="inner">
                    <h4>Data Mempelai</h4>
                    <p>
                        <?php if ($output['mempelai']) { ?>
                        <i class="ion ion-checkmark"></i>&nbsp;
                        <?php } ?>
                        {{($output['mempelai']) ? 'Data sudah siap' : 'Data masih kosong'}}
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="{{url('/mempelai')}}" class="small-box-footer">Buka data mempelai <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-3 ">
            <div class="small-box bg-secondary">
                <div class="inner">
                    <h4>Data Tamu</h4>
                    <p>
                        <?php if ($output['tamu']) { ?>
                        <i class="ion ion-checkmark"></i>&nbsp;
                        <?php } ?>
                        {{($output['tamu']) ? 'Data sudah siap' : 'Data masih kosong'}}
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                </div>
                <a href="{{url('/tamu')}}" class="small-box-footer">Buka data tamu <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-3 ">
            <div class="small-box bg-primary">
                <div class="inner">
                    <h4>Data Undangan</h4>
                    <p>
                        <?php if ($output['undangan']) { ?>
                        <i class="ion ion-checkmark"></i>&nbsp;
                        <?php } ?>
                        {{($output['undangan']) ? 'Data sudah siap' : 'Data masih kosong'}}
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-email"></i>
                </div>
                <a href="{{url('/undangan')}}" class="small-box-footer">Buka data undangan <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-3 ">
            <div class="small-box bg-danger">
                <div class="inner">
                    <h4>Data Media</h4>
                    <p><?php if ($output['media']) { ?>
                        <i class="ion ion-checkmark"></i>&nbsp;
                        <?php } ?>
                        {{($output['media']) ? 'Data sudah siap' : 'Data masih kosong'}}
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-images"></i>
                </div>
                <a href="{{url('/media')}}" class="small-box-footer">Buka data media <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 ">
            <div class="small-box bg-success">
                <div class="inner">
                    <h4>Undangan {{$output['user']}}</h4>
                    <p>{{($output['undangan_anda']) ? 'Semua data undangan sudah siap' : 'Undangan belum siap'}}</p>
                </div>
                <div class="icon">
                    <i class="ion ion-paper-airplane"></i>
                </div>
                <?php if ($output['undangan_anda']) { ?>
                <a href="{{url('/undangan-anda')}}" class="small-box-footer">Lihat undangan anda <i
                        class="fas fa-arrow-circle-right"></i></a>
                <?php } ?>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

    </script>
@endpush



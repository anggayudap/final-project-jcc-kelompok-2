@extends('layout.admin.master')
@section('title-menu')
    Halaman Tambah Undangan
@endsection
@section('content')
<form action="/undangan/{{$undangan->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="tanggal">Tanggal</label>
        <input type="date" class="form-control" name="tanggal" id="tanggal" value="{{($undangan) ? $undangan->tanggal : ''}}" placeholder="Masukkan Tanggal">
        @error('tanggal')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
         @enderror
    </div>

    <div class="form-group">
        <label for="waktu_mulai">Waktu Mulai</label>
        <input type="text" class="form-control" name="waktu_mulai" id="waktu-mulai" value="{{($undangan) ? $undangan->waktu_mulai : ''}}" placeholder="Masukkan Waktu Mulai">
        @error('waktu_mulai')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
         @enderror
    </div>

    <div class="form-group">
        <label for="waktu_selesai">Waktu Selesai</label>
        <input type="text" class="form-control" name="waktu_selesai" id="waktu-selesai" value="{{($undangan) ? $undangan->waktu_selesai : ''}}" placeholder="Masukkan Waktu Selesai">
        @error('waktu_selesai')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
         @enderror
    </div>

    <div class="form-group">
        <label for="alamat">Alamat</label>
        <input type="text" class="form-control" name="alamat" id="alamat" value="{{($undangan) ? $undangan->alamat : ''}}" placeholder="Masukkan Alamat">
        @error('alamat')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
         @enderror
    </div>


    <div class="form-group">
        <label for="longtitude_maps">Longtitude Maps</label>
        <input type="text" class="form-control" name="longtitude_maps" id="longtitude_maps" value="{{($undangan) ? $undangan->longtitude_maps : ''}}" placeholder="Masukkan Longtitude Maps">
        @error('longtitude_maps')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
         @enderror
    </div>

    <div class="form-group">
        <label for="latitude_maps">Latitude Maps</label>
        <input type="text" class="form-control" name="latitude_maps" id="latitude_maps" value="{{($undangan) ? $undangan->latitude_maps : ''}}" placeholder="Masukkan Latitude Maps">
        @error('latitude_maps')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
         @enderror
    </div>

    <button type="submit" class="btn btn-primary">Simpan</button>

</form>

@endsection

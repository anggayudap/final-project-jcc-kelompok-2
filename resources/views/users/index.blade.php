@extends('layout.admin.master')
@section('title-menu')
    Halaman Update User
@endsection
@section('content')
<form action="/users/{{$users->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
        <label for="name">Full Name</label>
        <input type="text" class="form-control" name="name" value="{{$users->name}}" id="name" placeholder="{{ __('Full Name')}}">
        @error('name')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
         @enderror
    </div>

    <div class="form-group">
        <label for="email">E-Mail Address</label>
        <input type="email" class="form-control" name="email" value="{{$users->email}}" id="emai" placeholder="{{ __('E-Mail Address')}}">
        @error('email')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
         @enderror
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password" placeholder="{{ __('New Password') }}">
        @error('password')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>

    <div class="form-group">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
    </div>

    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection
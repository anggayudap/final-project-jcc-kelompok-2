@extends('layout.admin.master-auth')
@section('auth-content')
    <div class="card-body login-card-body">
        <p class="login-box-msg">Silahkan Masukan Email dan Password</p>

        <form action="{{ route('login') }}" method="post">
        @csrf
            <div class="input-group mb-3">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}" >
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="input-group mb-3">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="icheck-primary">
                        <input type="checkbox" id="remember">
                        <label for="remember">
                            Ingat Saya
                        </label>
                    </div>
                </div>
            </div>
            <div class="social-auth-links text-center mb-3">
                <button type="submit" class="btn btn-block btn-primary">
                    <i class="fas fa-sign-in-alt mr-2"></i>
                </button>
                <p class="mt-1 mb-1">- OR -</p>
                <a href="{{ route('register') }}" class="btn btn-block btn-success">
                    <i class="fas fa-user-plus mr-2"></i> Daftar Baru
                </a>
            </div>
        </div>
        </form>
@endsection

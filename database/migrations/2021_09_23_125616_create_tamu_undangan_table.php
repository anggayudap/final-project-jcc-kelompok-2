<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTamuUndanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tamu_undangan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_tamu');
            $table->string('alamat_tamu');
            $table->string('nomor_hp_tamu');
            $table->unsignedBigInteger('id_undangan');
            $table->foreign('id_undangan')->references('id')->on('undangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tamu_undangan');
    }
}

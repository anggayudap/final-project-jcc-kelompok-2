<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKontenUndanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konten_undangan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type' , ['foto', 'video']);
            $table->string('filename');
            $table->unsignedBigInteger('id_undangan');
            $table->foreign('id_undangan')->references('id')->on('undangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konten_undangan');
    }
}

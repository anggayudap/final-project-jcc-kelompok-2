<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMempelaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mempelai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_mempelai');
            $table->string('nama_lengkap_mempelai');
            $table->enum('jenis_kelamin', ['laki-laki', 'perempuan']);
            $table->string('nama_bapak_mempelai');
            $table->string('nama_ibu_mempelai');
            $table->string('status_anak');
            $table->unsignedBigInteger('id_undangan');
            $table->foreign('id_undangan')->references('id')->on('undangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mempelai');
    }
}
